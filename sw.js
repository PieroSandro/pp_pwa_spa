self.addEventListener("install",e=>{
    // console.log("installed decode!");
    e.waitUntil(
        caches.open("pwa_spa_p1").then(cache=>{
            return cache.addAll([
              
              "./php/index.php",

            "./html/index.html",
           
           
            "./php/controladores/vistasControlador.php",
            "./php/modelos/vistasModelo.php",
            "./php/vistas/plantilla.php",
            
           

            "https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js",
            "https://unpkg.com/vue-router/dist/vue-router.js","./recursos/img/logo192.png"]);
          //  .then(() => self.skipWaiting())
        })
      //  .catch(err => console.log('Falló registro de cache', err))
    );
 });
 
 self.addEventListener("fetch",e=>{
  //console.log("simom.. "+e.request.url);
 console.log(`interceptando fetch request for: ${e.request.url}`);
 e.respondWith(
     caches.match(e.request).then(response=>{
         return response || fetch(e.request);
         
     })
 );
 });