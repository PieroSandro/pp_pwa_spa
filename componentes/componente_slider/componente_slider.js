Vue.component('componente_slider',{
    template:/*html */
    `<div class="row" style="font-family: 'Montserrat', sans-serif;">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li id="libullet_1" data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li id="libullet_2" data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li id="libullet_3" data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" v-for="imagen in imagenes">
      <div class="carousel-item active">
        <img :src="imagen.imagen_1" class="d-block img-fluid" alt="">
      </div>
      <div class="carousel-item">
        <img :src="imagen.imagen_2" class="d-block img-fluid" alt="">
      </div>
      <div class="carousel-item">
        <img :src="imagen.imagen_3" class="d-block img-fluid" alt="">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div><!--FIN DEL SLIDER-->
    
    </div>`,

    data(){
        return{
            imagenes:[
                {
                 imagen_1:'recursos/img/chicos.jpg', alt:'computadora',
                 imagen_2:'recursos/img/lomas.jpg', alt:'desarrollo',
                 imagen_3:'recursos/img/natu.jpg', alt:'programador',
                }
            ]
        }
    }
    
})