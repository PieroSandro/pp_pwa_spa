const nosotros=Vue.component('componente_nosotros',{
    template:/*html */
    `
    <div class="container" 
    style="padding-left:0px; padding-right:0px; padding-bottom:0px; padding-top:20px;">
    <div class="row">
    <div class="col-md-6">
     <h3 class="text-center text-primary" id="mision">
         Misión
       </h3>
       <p class="text-justify">
         Promover el desarrollo y la innovacion tecnologica en nuestros clientes brindando soluciones 
         acorde a sus necesidades para el cumplimiento de sus objetivos.
       </p>
    </div>
    <div class="col-md-6">
        <img src="recursos/img/mi_mision.jpg" class="img-fluid" alt="">
    </div>
 </div>
 
 <div class="row">
 <div class="col-md-6">
     <img src="recursos/img/mi_visiom.png" class="img-fluid" alt="">
 </div>
 <div class="col-md-6">
     <h3 class="text-center text-primary" id="vision">
         Visión
       </h3>
       <p class="text-justify">
         Ser una empresa reconocida a nivel nacional por la contribución al 
         progreso tecnologico y por el desarrollo de soluciones a largo plazo.       
       </p>
 </div>
 </div>          
 
 <h3 class="text-center text-primary" id="valores">
 Valores
 </h3>

</div>
`,

data(){
    return{

    }
}
})
