const inicio=Vue.component('componente_inicio',{
    template:/*html */
    `
    <div class="container" 
    style="padding-left:0px; padding-right:0px; padding-bottom:0px; padding-top:20px;">
<div class="row">
      <div class="col-md-6">
        <img src="recursos/img/ste3.jpg" class="img-fluid columna" alt="ste3">
      </div>
      <div class="col-md-6 bg_plomo">
        <h3 class="text-center text-primary" id="quienes_somos">
          ¿QUIÉNES SOMOS?
        </h3>
        <p class="text-justify">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo veritatis nesciunt quas, sunt at debitis 
          commodi ratione amet aliquam. Fugit perspiciatis obcaecati architecto nisi nemo, nesciunt ipsam dicta quisquam 
          necessitatibus.Quia quis ullam in veritatis incidunt dolore? Reiciendis exercitationem magnam optio inventore beatae 
          debitis consectetur molestias iste dolorem.
           Provident modi odit, architecto dolorem dolor aperiam ratione eaque molestiae inventore delectus.
        </p>
      </div>
    </div>
<br>
    <div class="row">
      <div class="col-md-6">
        <img src="recursos/img/ste3.jpg" class="img-fluid" alt="ste3">
      </div>
      <div class="col-md-6 bg_plomo">
        <h3 class="text-center text-primary" id="que_hacemos">
          ¿QUÉ HACEMOS?
        </h3>
        <p class="text-justify">
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quo veritatis nesciunt quas, sunt at debitis 
          commodi ratione amet aliquam. Fugit perspiciatis obcaecati architecto nisi nemo, nesciunt ipsam dicta quisquam 
          necessitatibus.Quia quis ullam in veritatis incidunt dolore? Reiciendis exercitationem magnam optio inventore beatae 
          debitis consectetur molestias iste dolorem.
           Provident modi odit, architecto dolorem dolor aperiam ratione eaque molestiae inventore delectus.
        </p>
      </div>
    </div>
<hr>

<div class="imagen_aplicaion_web  pt-5">
<div class="container">
<div class="row ">
  <div class="col-md-6"><br>
  
      <div class="text-white pl-3" v-for=" (servicio, index) in servicios">
      <center class="wow animated fadeInUp"> {{ servicio.servis1 }} - {{ servicio.servis2 }} - {{ servicio.servis3 }}</center><br>
      <center class="wow animated animate__slideInLeft delay-1s">{{ servicio.servis4 }} - {{ servicio.servis5 }}</center>
      </div>
  </div>
  <div class="col-md-6">

  </div>
</div>
</div>
</div>

<br>

<div class="row" v-for="aplicacion in aplicaciones">
<div class="col-md-6 pt-5">
<h3 id="titleh3" style="color: rgb(27, 27, 93); text-align:center; font-family: Montserrat, sans-serif; width:100%;" 
:class="[aplicacion.animated_wow_2, 'pt-3']">{{ aplicacion.titulo }}</h3>
 <p :class="[aplicacion.animated_wow_1, 'text-justify']">{{ aplicacion.descripcion }}</p>
</div>
<div class="col-md-6 escala">
  <img :src="aplicacion.imagen" :class="[aplicacion.animated_wow_2, 'img-fluid pt-3']" :alt="aplicacion.alt"><hr>
</div>
</div>

</div>
`,

    data(){
        return{
            servicios:[
                {
                  servis1:'APLICACIONES WEB', 
                  servis2:'SITIOS WEB A MEDIDA', 
                  servis3:'ANIMADAS Y DINÁMICAS', 
                  servis4:'RESPONSIVE WEB DESIGN', 
                  servis5:'SEO'},
              ],
  
              
              aplicaciones:[
                {
                  imagen:'recursos/img/aplica_webs.png', 
                  titulo:'DASHBOARD', 
                  alt:'aplicaciones web',
                  descripcion:'En terminos sencillos un dashboard es una pagina desde donde puedes ver indicadores del rendimiento o la evolucion de tu negocio. Estos indicadores o resumenes te permitiran ver cual es la situacion actual de tu organizacion y tomar desiciones que la lleven por buen camino. Nosotros te frecemos esta opcion gracias al analisis que hacemos de tus requerimientos y un adecuado analisis que nos permitira entregarte lo que necesitas.', 
                  animated_wow_1:'wow animated fadeInUp delay-1s',
                  animated_wow_2:'wow animated fadeInUp'
                },
                {
                  imagen:'recursos/img/sistios_web_a_medida.png', 
                  titulo:'SITIOS WEB A MEDIDA', 
                  alt:'sitios web a medida',
                  descripcion:' En el momento en que tu emprendimiento requiera de un sitio web o una aplicación a las que puedas acceder tu y tus clientes desde Internet, y no tengas una idea clara de como implementarla; debes saber que nosotros podemos construirte una solución que se ajuste a la medida de lo que requieres . Nosotros te ofrecemos justo lo que necesitas pero con la capacidad de escalar o ampliar tu proyecto si lo requieres en algún momento.',
                  animated_wow_1:'wow animated fadeInUp delay-1s',
                  animated_wow_2:'wow animated fadeInUp'
                },
                {
                  imagen:'recursos/img/tecnologias-de-desarrollo-web.jpg', 
                  titulo:'ANIMADAS Y DINÁMICAS', 
                  alt:'animadas y dinamicas',
                  descripcion:'hola como estas', 
                  animated_wow_2:'wow animated fadeInUp'
                },
                {
                  imagen:'recursos/img/que-es-responsive-web-design.jpg', 
                  titulo:'RESPONSIVE WEB DESIGN', 
                  alt:'responsive web design',
                  descripcion:'Los tamaños de los dispositivos desde donde se accede a un sitio web son variados, debido a esto un sitio web debe estar diseñado de tal forma que su contenido se adapte a cualquier dispositivo conservando su fácil visualización. Nosotros te ofrecemos un proyecto que cumpla con esta característica para que tus clientes encuentren rápidamente lo que  buscan.', 
                  animated_wow_2:'wow animated fadeInUp'
                },
                {
                  imagen:'recursos/img/optimizacion_SEO.jpg', 
                  titulo:'SEO', 
                  alt:'seo',
                  descripcion:'Si tu objetivo es que tu sitio web logre destacar en Internet ubicandose entre los primeros resultados cuando un cliente busca en la web productos que afreces, entonces podemos ayudarte. Para lograr este objetivo existen muchas tecnicas que en conjunto se denominan SEO (del inglés Search Engine Optimization) o posicionamiento en buscadores, en español. Con estas herramientas podemos hacer que figures entre los primeros resultados en Internet de tal forma que mas personas vean tus productos o servicios.', 
                  animated_wow_2:'wow animated fadeInUp'
                },
  
              ]
        }
    }
})
