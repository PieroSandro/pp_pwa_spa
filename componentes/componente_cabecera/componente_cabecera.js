//https://codepen.io/intotheprogram/pen/ZjxZdg   TUTORIAL EFECTO MOUSEOVER MOUSEAUT
Vue.component('componente_header', {
    template:/*html*/
    `
   <div class="row" id="top" style="font-family: 'Montserrat', sans-serif;">
    <nav id="navbar-top" class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  
      <a class="navbar-brand" href="#"><img src="recursos/img/logo512.png" alt="imagen_logo" 
     width="40" height="40" class="img-fluid"></a>
     <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" 
      data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
      aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>-->
      <!------------->
      <div class="c3" data-toggle="collapse" data-target="#navbarSupportedContent" onclick="mf(this)">
    
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
           
            </div>

      <!------------->
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
           <!-- <a class="nav-link text-info animated fadeInRight" id="inicio" href="http://localhost/techdevs_vue/principal">-->
            <!--INICIO-->
            <!--28_12_2020-------------------------------------------->
            <a class="nav-link text-info animated fadeInRight" id="inicio">
            <router-link to="/inicio">INICIO</router-link> 
            <!--28_12_2020-------------------------------------------->
            </a>
          </li>
          <li class="nav-item">
           <!-- <a class="nav-link text-info animated fadeInRight" 
            id="nosotros" href="http://localhost/techdevs_vue/nosotros">NOSOTROS</a>-->
            <!--28_12_2020-------------------------------------------->
            <a class="nav-link text-info animated fadeInRight" id="nosotros">
            <router-link to="/nosotros">NOSOTROS</router-link> </a>
            <!--28_12_2020-------------------------------------------->
          </li>
     
          <li class="nav-item">
          <a class="nav-link text-info  animated fadeInRight" 
          id="servicios" href="#" tabindex="-1" aria-disabled="true">SERVICIOS</a>
        </li>
          <li class="nav-item">
            <a class="nav-link text-info  animated fadeInRight" 
            id="portafolio" href="#" tabindex="-1" aria-disabled="true">PORTAFOLIO</a>
          </li>

          <li class="nav-item">
            <a class="nav-link text-info  animated fadeInRight" 
            id="contacto" href="#" tabindex="-1" aria-disabled="true">CONTACTO</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">

          <div id="buscador" class="input-group animated fadeInRight">
            <input type="text" id="input_buscar" class="form-control" placeholder="Buscar..." 
            aria-label="Recipient's username" aria-describedby="button-addon2">
            <div class="input-group-append">
             <button class="btn" type="button" 
             id="button-addon2"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </form>
      </div>
   
  </nav>
  </div>



  
    `,

    data(){
        return{
          servicios:true,
          servis:true,
        }
    },

    methods: {
      mouseover(){
        this.servicios = !this.servicios;
      },    
      /*mouseleave(){
        this.servicios = !this.servicios;      
      }*/

      servicios_mostrar_ocultar(){
        this.servis = !this.servis;
      }
    }
});
